
def print_error(data, preset = '[!] '):
	if isinstance(data, str) or isinstance(data, int) or isinstance(data, float):
		print(preset + str(data))
	elif isinstance(data, list):
		for elem in data:
			print(preset + str(elem))

def print_info(data, preset = '[i] '):
	if isinstance(data, str) or isinstance(data, int) or isinstance(data, float):
		print(preset + str(data))
	elif isinstance(data, list):
		for elem in data:
			print(preset + str(elem))
