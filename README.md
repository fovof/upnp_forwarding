# README #

UPnP forwarding vulnerability scanner.


### Usage ###

* Make a list of IPs.
* To do all steps run: 

		run.py -s -p -t -iF IPs.txt
		
* If you want to do steps separetly:

		-s, --search: checkes if xml file is available
	
		-p, --parse: tries to parse xml
	
		-t, --transfer: does the port mapping 
	
	

* To delete port mapping use -d, --deny

* Configuration:

		-th, --threads N: set N threads, default: 5

		-port, --portforward N: inner port to forward, default: 80